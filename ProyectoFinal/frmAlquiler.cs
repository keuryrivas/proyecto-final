﻿using ProyectoFinal.DataModel.Entities;
using ProyectoFinal.Repositorio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoFinal
{
    public partial class frmAlquiler : Form
    {
        AlquilerRepository alq = new AlquilerRepository();
        public frmAlquiler()
        {
            InitializeComponent();
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void domainUpDown1_SelectedItemChanged(object sender, EventArgs e)
        {
          /*  DomainUpDown.DomainUpDownItemCollection domainUp = this.domainUpDown1.Items;

            domainUp.Add("Efectivo");
            domainUp.Add("Tarjeta");

            this.domainUpDown1.Text = "PAGO";*/

          

        }

        private void frmAlquiler_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'proyectoFinalDataSet5.Alquiler' Puede moverla o quitarla según sea necesario.
            this.alquilerTableAdapter.Fill(this.proyectoFinalDataSet5.Alquiler);

        }

        private void dtpFechaHasta_ValueChanged(object sender, EventArgs e)
        {

        }

        private void comboPago_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnAlquilar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtClienteID.Text) || string.IsNullOrWhiteSpace(txtVehiculoID.Text) || string.IsNullOrWhiteSpace(comboPago.Text) || string.IsNullOrWhiteSpace(txtCosto.Text) || string.IsNullOrWhiteSpace(dtpFechaDesde.Text) || string.IsNullOrWhiteSpace(dtpFechaHasta.Text) || string.IsNullOrWhiteSpace(dtpFechaDevolucion.Text) || string.IsNullOrWhiteSpace(dtpPenalidad.Text))
            {
                MessageBox.Show("Complete todos los campos.");
            }
            else
            {
                Alquiler alquiler = new Alquiler()
                {
                    ClienteId = Int32.Parse(txtClienteID.Text),
                    VehiculoId = Int32.Parse(txtVehiculoID.Text),
                    MetodoPago = comboPago.Items[comboPago.SelectedIndex].ToString(),
                    Costo = decimal.Parse(txtCosto.Text),
                    FechaDesde = Convert.ToDateTime(dtpFechaDesde.Text),
                    FechaHasta = Convert.ToDateTime(dtpFechaHasta.Text),
                    FechaDevolucion = Convert.ToDateTime(dtpFechaDevolucion.Text),
                    Penalidad = decimal.Parse(dtpPenalidad.Text),
                    Estatus = "A",
                    Borrado = false,
                    FechaRegistro = System.DateTime.Now,
                    FechaModificación = System.DateTime.Now
                };


                var created = alq.Create(alquiler);

                if (created.Estatus != null)
                {
                    MessageBox.Show("Exitoso");
                }
                else
                {
                    MessageBox.Show("Error creando la marca");
                }

                dgvAlquiler.DataSource = alq.GetAll();
            }
        }

        private void dgvAlquiler_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            textBoxId.Text = dgvAlquiler.CurrentRow.Cells["alquilerIdDataGridViewTextBoxColumn"].Value.ToString();
            txtClienteID.Text =  dgvAlquiler.CurrentRow.Cells["clienteIdDataGridViewTextBoxColumn"].Value.ToString();
            txtVehiculoID.Text =  dgvAlquiler.CurrentRow.Cells["vehiculoIdDataGridViewTextBoxColumn"].Value.ToString();
            comboPago.Text =  dgvAlquiler.CurrentRow.Cells["metodoPagoDataGridViewTextBoxColumn"].Value.ToString();
            txtCosto.Text =  dgvAlquiler.CurrentRow.Cells["costoDataGridViewTextBoxColumn"].Value.ToString();
            dtpFechaDesde.Text =  dgvAlquiler.CurrentRow.Cells["fechaDesdeDataGridViewTextBoxColumn"].Value.ToString();
            dtpFechaHasta.Text =  dgvAlquiler.CurrentRow.Cells["fechaHastaDataGridViewTextBoxColumn"].Value.ToString();
            dtpFechaDevolucion.Text =  dgvAlquiler.CurrentRow.Cells["fechaDevolucionDataGridViewTextBoxColumn"].Value.ToString();
            dtpPenalidad.Text =  dgvAlquiler.CurrentRow.Cells["penalidadDataGridViewTextBoxColumn"].Value.ToString();


        }

        private void btnBorrar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(textBoxId.Text))
            {
                MessageBox.Show("Complete todos los campos.");
            }
            else
            {

                var buscar = alq.FindById(Int32.Parse(textBoxId.Text));

                var created = alq.Delete(buscar);


                if (created.Success == true)
                {
                    MessageBox.Show("Exitoso");
                    dgvAlquiler.DataSource = alq.GetAll();
                }
                else
                {
                    MessageBox.Show("Error Actualizando");
                }
            }
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(textBoxId.Text))
            {
                MessageBox.Show("Complete todos los campos.");
            }
            else
            {
                Alquiler alquiler = new Alquiler()
                {
                    Id = Int32.Parse(textBoxId.Text),
                    ClienteId = Int32.Parse(txtClienteID.Text),
                    VehiculoId = Int32.Parse(txtVehiculoID.Text),
                    MetodoPago = comboPago.Items[comboPago.SelectedIndex].ToString(),
                    Costo = decimal.Parse(txtCosto.Text),
                    FechaDesde = Convert.ToDateTime(dtpFechaDesde.Text),
                    FechaHasta = Convert.ToDateTime(dtpFechaHasta.Text),
                    FechaDevolucion = Convert.ToDateTime(dtpFechaDevolucion.Text),
                    Penalidad = decimal.Parse(dtpPenalidad.Text),
                    Estatus = "A",
                    Borrado = false,
                    FechaRegistro = System.DateTime.Now,
                    FechaModificación = System.DateTime.Now
                };

                var created = alq.UpdateAlquiler(alquiler);


                if (created.Success == true)
                {
                    MessageBox.Show("Exitoso");
                    dgvAlquiler.DataSource = alq.GetAll();
                }
                else
                {
                    MessageBox.Show("Error Actualizando");
                }
                dgvAlquiler.DataSource = alq.GetAll();
            }
        }
    }
}
