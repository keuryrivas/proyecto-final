﻿namespace ProyectoFinal
{
    partial class frmCombustible
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.btnAgregar = new System.Windows.Forms.Button();
            this.btnActualizar = new System.Windows.Forms.Button();
            this.btnBorrar = new System.Windows.Forms.Button();
            this.dgvCombustible = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxId = new System.Windows.Forms.TextBox();
            this.proyectoFinalDataSet3 = new ProyectoFinal.ProyectoFinalDataSet3();
            this.tipoTransmisionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tipoTransmisionTableAdapter = new ProyectoFinal.ProyectoFinalDataSet3TableAdapters.TipoTransmisionTableAdapter();
            this.proyectoFinalDataSet6 = new ProyectoFinal.ProyectoFinalDataSet6();
            this.tipoCombustibleBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tipoCombustibleTableAdapter = new ProyectoFinal.ProyectoFinalDataSet6TableAdapters.TipoCombustibleTableAdapter();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nombreDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.estatusDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fechaRegistroDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fechaModificaciónDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCombustible)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.proyectoFinalDataSet3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tipoTransmisionBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.proyectoFinalDataSet6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tipoCombustibleBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nombre";
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(76, 48);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(100, 22);
            this.txtNombre.TabIndex = 1;
            // 
            // btnAgregar
            // 
            this.btnAgregar.Location = new System.Drawing.Point(177, 185);
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Size = new System.Drawing.Size(75, 34);
            this.btnAgregar.TabIndex = 2;
            this.btnAgregar.Text = "Agregar";
            this.btnAgregar.UseVisualStyleBackColor = true;
            this.btnAgregar.Click += new System.EventHandler(this.btnAgregar_Click);
            // 
            // btnActualizar
            // 
            this.btnActualizar.Location = new System.Drawing.Point(96, 185);
            this.btnActualizar.Name = "btnActualizar";
            this.btnActualizar.Size = new System.Drawing.Size(75, 34);
            this.btnActualizar.TabIndex = 3;
            this.btnActualizar.Text = "Actualizar";
            this.btnActualizar.UseVisualStyleBackColor = true;
            this.btnActualizar.Click += new System.EventHandler(this.btnActualizar_Click);
            // 
            // btnBorrar
            // 
            this.btnBorrar.Location = new System.Drawing.Point(15, 185);
            this.btnBorrar.Name = "btnBorrar";
            this.btnBorrar.Size = new System.Drawing.Size(75, 34);
            this.btnBorrar.TabIndex = 4;
            this.btnBorrar.Text = "Borrar";
            this.btnBorrar.UseVisualStyleBackColor = true;
            this.btnBorrar.Click += new System.EventHandler(this.btnBorrar_Click);
            // 
            // dgvCombustible
            // 
            this.dgvCombustible.AutoGenerateColumns = false;
            this.dgvCombustible.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCombustible.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.nombreDataGridViewTextBoxColumn,
            this.estatusDataGridViewTextBoxColumn,
            this.fechaRegistroDataGridViewTextBoxColumn,
            this.fechaModificaciónDataGridViewTextBoxColumn});
            this.dgvCombustible.DataSource = this.tipoCombustibleBindingSource;
            this.dgvCombustible.Location = new System.Drawing.Point(15, 237);
            this.dgvCombustible.Name = "dgvCombustible";
            this.dgvCombustible.RowHeadersWidth = 51;
            this.dgvCombustible.RowTemplate.Height = 24;
            this.dgvCombustible.Size = new System.Drawing.Size(763, 200);
            this.dgvCombustible.TabIndex = 5;
            this.dgvCombustible.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCombustible_CellContentClick);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(21, 17);
            this.label2.TabIndex = 6;
            this.label2.Text = "ID";
            // 
            // textBoxId
            // 
            this.textBoxId.Location = new System.Drawing.Point(76, 9);
            this.textBoxId.Name = "textBoxId";
            this.textBoxId.ReadOnly = true;
            this.textBoxId.Size = new System.Drawing.Size(100, 22);
            this.textBoxId.TabIndex = 7;
            // 
            // proyectoFinalDataSet3
            // 
            this.proyectoFinalDataSet3.DataSetName = "ProyectoFinalDataSet3";
            this.proyectoFinalDataSet3.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tipoTransmisionBindingSource
            // 
            this.tipoTransmisionBindingSource.DataMember = "TipoTransmision";
            this.tipoTransmisionBindingSource.DataSource = this.proyectoFinalDataSet3;
            // 
            // tipoTransmisionTableAdapter
            // 
            this.tipoTransmisionTableAdapter.ClearBeforeFill = true;
            // 
            // proyectoFinalDataSet6
            // 
            this.proyectoFinalDataSet6.DataSetName = "ProyectoFinalDataSet6";
            this.proyectoFinalDataSet6.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tipoCombustibleBindingSource
            // 
            this.tipoCombustibleBindingSource.DataMember = "TipoCombustible";
            this.tipoCombustibleBindingSource.DataSource = this.proyectoFinalDataSet6;
            // 
            // tipoCombustibleTableAdapter
            // 
            this.tipoCombustibleTableAdapter.ClearBeforeFill = true;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "TipoCombustibleId";
            this.dataGridViewTextBoxColumn1.HeaderText = "TipoCombustibleId";
            this.dataGridViewTextBoxColumn1.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 125;
            // 
            // nombreDataGridViewTextBoxColumn
            // 
            this.nombreDataGridViewTextBoxColumn.DataPropertyName = "Nombre";
            this.nombreDataGridViewTextBoxColumn.HeaderText = "Nombre";
            this.nombreDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.nombreDataGridViewTextBoxColumn.Name = "nombreDataGridViewTextBoxColumn";
            this.nombreDataGridViewTextBoxColumn.Width = 125;
            // 
            // estatusDataGridViewTextBoxColumn
            // 
            this.estatusDataGridViewTextBoxColumn.DataPropertyName = "Estatus";
            this.estatusDataGridViewTextBoxColumn.HeaderText = "Estatus";
            this.estatusDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.estatusDataGridViewTextBoxColumn.Name = "estatusDataGridViewTextBoxColumn";
            this.estatusDataGridViewTextBoxColumn.Width = 125;
            // 
            // fechaRegistroDataGridViewTextBoxColumn
            // 
            this.fechaRegistroDataGridViewTextBoxColumn.DataPropertyName = "FechaRegistro";
            this.fechaRegistroDataGridViewTextBoxColumn.HeaderText = "FechaRegistro";
            this.fechaRegistroDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.fechaRegistroDataGridViewTextBoxColumn.Name = "fechaRegistroDataGridViewTextBoxColumn";
            this.fechaRegistroDataGridViewTextBoxColumn.Width = 125;
            // 
            // fechaModificaciónDataGridViewTextBoxColumn
            // 
            this.fechaModificaciónDataGridViewTextBoxColumn.DataPropertyName = "FechaModificación";
            this.fechaModificaciónDataGridViewTextBoxColumn.HeaderText = "FechaModificación";
            this.fechaModificaciónDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.fechaModificaciónDataGridViewTextBoxColumn.Name = "fechaModificaciónDataGridViewTextBoxColumn";
            this.fechaModificaciónDataGridViewTextBoxColumn.Width = 125;
            // 
            // frmCombustible
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.textBoxId);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dgvCombustible);
            this.Controls.Add(this.btnBorrar);
            this.Controls.Add(this.btnActualizar);
            this.Controls.Add(this.btnAgregar);
            this.Controls.Add(this.txtNombre);
            this.Controls.Add(this.label1);
            this.Name = "frmCombustible";
            this.Text = "frmCombustible";
            this.Load += new System.EventHandler(this.frmCombustible_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCombustible)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.proyectoFinalDataSet3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tipoTransmisionBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.proyectoFinalDataSet6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tipoCombustibleBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Button btnAgregar;
        private System.Windows.Forms.Button btnActualizar;
        private System.Windows.Forms.Button btnBorrar;
        private System.Windows.Forms.DataGridView dgvCombustible;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxId;
        private ProyectoFinalDataSet3 proyectoFinalDataSet3;
        private System.Windows.Forms.BindingSource tipoTransmisionBindingSource;
        private ProyectoFinalDataSet3TableAdapters.TipoTransmisionTableAdapter tipoTransmisionTableAdapter;
        private ProyectoFinalDataSet6 proyectoFinalDataSet6;
        private System.Windows.Forms.BindingSource tipoCombustibleBindingSource;
        private ProyectoFinalDataSet6TableAdapters.TipoCombustibleTableAdapter tipoCombustibleTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombreDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn estatusDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fechaRegistroDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fechaModificaciónDataGridViewTextBoxColumn;
    }
}