﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoFinal
{
    public partial class Mantenimiento : Form
    {
        public Mantenimiento()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmCliente cliente = new frmCliente();
            cliente.ShowDialog();

        }

        private void button3_Click(object sender, EventArgs e)
        {
            frmMarca fmarca = new frmMarca();
            fmarca.ShowDialog();
        }

        private void Mantenimiento_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmVehiculo fvehiculo = new frmVehiculo();
            fvehiculo.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            frmModelo modelo = new frmModelo();
                modelo.ShowDialog();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            frmCombustible cmb = new frmCombustible();
            cmb.ShowDialog();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            frmTransmision trm = new frmTransmision();
            trm.ShowDialog();
        }
    }
}
