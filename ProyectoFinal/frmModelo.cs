﻿using ProyectoFinal.DataModel.Entities;
using ProyectoFinal.Repositorio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoFinal
{


    public partial class frmModelo : Form
    {
        ModeloRepository _modelo = new ModeloRepository();
        MarcaRepository _marca = new MarcaRepository();
        public frmModelo()
        {
            InitializeComponent();
            dgvModelo.DataSource = _modelo.GetAll();
        }

        private void txtMarcaID_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void domainUpDown1_SelectedItemChanged(object sender, EventArgs e)
        {
          
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(textMarcaID.Text))
            {
                MessageBox.Show("Complete el campo para buscar.");
            }
            else
            {

                var buscar = _marca.FindById(Int32.Parse(textMarcaID.Text));


                if (buscar != null)
                {
                    textMNombre.Text = buscar.Nombre;
                    textMarcaID.Text = buscar.Id.ToString();
                    dgvModelo.DataSource = _modelo.GetAll();
                }
                else
                {
                    MessageBox.Show("Marca no encontrada");
                }
            }
        }

        private void dgvModelo_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            txtNombre.Text = dgvModelo.CurrentRow.Cells["nombreDataGridViewTextBoxColumn"].Value.ToString();
            //textBoxId.Text = dgvModelo.CurrentRow.Cells["ModeloId"].Value.ToString();
            textMarcaID.Text = dgvModelo.CurrentRow.Cells["marcaIdDataGridViewTextBoxColumn"].Value.ToString();
           // textMNombre.Text = dgvModelo.CurrentRow.Cells["modeloIdDataGridViewTextBoxColumn"].Value.ToString();
        }

        private void frmModelo_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'proyectoFinalDataSet1.Modelo' Puede moverla o quitarla según sea necesario.
            this.modeloTableAdapter1.Fill(this.proyectoFinalDataSet1.Modelo);
            // TODO: esta línea de código carga datos en la tabla 'proyectoFinalDataSet.Modelo' Puede moverla o quitarla según sea necesario.
            this.modeloTableAdapter.Fill(this.proyectoFinalDataSet.Modelo);

        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtNombre.Text) && string.IsNullOrWhiteSpace(textMarcaID.Text))
            {
                MessageBox.Show("Complete todos los campos.");
            }
            else
            {
                Modelo modelo = new Modelo()
                {

                    MarcaId = Int32.Parse(textMarcaID.Text),
                    Nombre = txtNombre.Text,
                    Estatus = "A",
                    Borrado = false,
                    FechaRegistro = System.DateTime.Now,
                    FechaModificación = System.DateTime.Now
                };


                var created = _modelo.Create(modelo);

                if (created.Nombre != null)
                {
                    MessageBox.Show("Exitoso");
                }
                else
                {
                    MessageBox.Show("Error creando la marca");
                }
                dgvModelo.DataSource = _modelo.GetAll();
            }
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtNombre.Text) && string.IsNullOrWhiteSpace(textMarcaID.Text))
            {
                MessageBox.Show("Complete todos los campos.");
            }
            else
            {
                Modelo modelo = new Modelo()
                {
                    Id = Int32.Parse(textBoxId.Text),
                    MarcaId = Int32.Parse(textMarcaID.Text),
                    Nombre = txtNombre.Text,
                    Estatus = "A",
                    Borrado = false,
                    FechaRegistro = System.DateTime.Now,
                    FechaModificación = System.DateTime.Now
                };

                var created = _modelo.UpdateModelo(modelo);


                if (created.Success == true)
                {
                    MessageBox.Show("Exitoso");
                    dgvModelo.DataSource = _modelo.GetAll();
                }
                else
                {
                    MessageBox.Show("Error Actualizando");
                }

            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void btnBorrar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtNombre.Text))
            {
                MessageBox.Show("Complete todos los campos.");
            }
            else
            {

                var buscar = _modelo.FindById(Int32.Parse(textBoxId.Text));

                var created = _modelo.Delete(buscar);


                if (created.Success == true)
                {
                    MessageBox.Show("Exitoso");
                    dgvModelo.DataSource = _modelo.GetAll();
                }
                else
                {
                    MessageBox.Show("Error Actualizando");
                }
            }
        }

        private void fillByToolStripButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.modeloTableAdapter.FillBy(this.proyectoFinalDataSet.Modelo);
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

        }
    }
}
