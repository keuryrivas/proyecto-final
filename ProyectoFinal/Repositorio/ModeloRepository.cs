﻿using ProyectoFinal.DataModel.Entities;
using ProyectoFinal.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoFinal.Repositorio
{
    public class ModeloRepository : GenericRepository<Modelo>, IModeloRepository
    {
        public OperationResult UpdateModelo(Modelo model)
        {
            var Modelo = FindById(model.Id);

            if (Modelo == null)
            {
                return null;
            }

            Modelo.Nombre = model.Nombre;
            return Update(Modelo);
        }
    }
}
