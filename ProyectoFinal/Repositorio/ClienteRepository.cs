﻿using ProyectoFinal.DataModel.Entities;
using ProyectoFinal.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoFinal.Repositorio
{
    public class ClienteRepository : GenericRepository<Cliente>, IClienteRepository
    {
        public OperationResult UpdateCliente(Cliente model)
        {
            var client = FindById(model.Id);

            if (client == null)
            {
                return null;
            }

            client.Nombre = model.Nombre;
            return Update(client);
        }
    }
}
