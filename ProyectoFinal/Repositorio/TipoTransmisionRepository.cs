﻿using ProyectoFinal.DataModel.Entities;
using ProyectoFinal.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoFinal.Repositorio
{
    public class TipoTransmisionRepository : GenericRepository<TipoTransmision>, ITipoTransmisionRepository
    {
        public OperationResult UpdateTrans(TipoTransmision model)
        {
            var marca = FindById(model.Id);

            if (marca == null)
            {
                return null;
            }

            marca.Nombre = model.Nombre;
            return Update(marca);
        }
    }
}
