﻿using ProyectoFinal.DataModel.Context;
using ProyectoFinal.DataModel.Entities;
using ProyectoFinal.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoFinal.Repositorio
{

    public class AlquilerRepository : GenericRepository<Alquiler>, IAlquilerRepository
    {
        public OperationResult UpdateAlquiler(Alquiler model)
        {
            {
                var alquiler = FindById(model.Id);

                if (alquiler == null)
                {
                    return null;
                }

                alquiler.Id = model.Id;
                alquiler.ClienteId = model.ClienteId;
                alquiler.VehiculoId = model.VehiculoId;
                alquiler.MetodoPago = model.MetodoPago;
                alquiler.Penalidad = model.Penalidad;
                alquiler.FechaDesde = model.FechaDesde;
                alquiler.FechaHasta = model.FechaHasta;
                alquiler.FechaDevolucion = model.FechaDevolucion;
                alquiler.Costo = model.Costo;
                
                return Update(alquiler);
            }
        }
    }
}
