﻿using ProyectoFinal.DataModel.Context;
using ProyectoFinal.DataModel.Entities;
using ProyectoFinal.Interface;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoFinal.Repositorio
{
    public class GenericRepository<T> : IGenericRepository<T> where T : BaseEntity
    {
        private MyAppDbContext dbContext;
        private DbSet<T> _set;

        public GenericRepository()
        {
            dbContext = new MyAppDbContext();
            _set = dbContext.Set<T>();
        }
        public T Create(T model)
        {
            _set.Add(model);
            if (model == null)
            {
                return null;
            }

            dbContext.SaveChanges();

            return model;
        }

        public OperationResult Delete(T model)
        {
            dbContext.Entry(model).State = EntityState.Modified;
            model.Borrado = true;
            dbContext.SaveChanges();

            return new OperationResult() { Success = true };
        }

        public T FindById(int id)
        {
            return _set.FirstOrDefault(x => x.Id == id);
        }

        public List<T> GetAll()
        {
            return _set.Where(x => x.Borrado == false).ToList();
        }

        public OperationResult Update(T model)
        {
            dbContext.Entry(model).State = EntityState.Modified;
            dbContext.SaveChanges();

            return new OperationResult() { Success = true };
        }
    }
}
