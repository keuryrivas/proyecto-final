﻿using ProyectoFinal.DataModel.Entities;
using ProyectoFinal.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoFinal.Repositorio
{
    public class VehiculoRepository : GenericRepository<Vehiculo>, IVehiculoRepository
    {
        public OperationResult UpdateVehiculo(Vehiculo model)
        {
            {
                var vehiculo = FindById(model.Id);

                if (vehiculo == null)
                {
                    return null;
                }

                vehiculo.Chasis = model.Chasis;
                return Update(vehiculo);
            }
        }
    }
}
