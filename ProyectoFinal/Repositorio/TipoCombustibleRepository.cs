﻿using ProyectoFinal.DataModel.Entities;
using ProyectoFinal.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoFinal.Repositorio
{
    public class TipoCombustibleRepository : GenericRepository<TipoCombustible>, ITipoCombustibleRepository
    {
        public OperationResult UpdateCombustible(TipoCombustible model)
        {
            var comb = FindById(model.Id);

            if (comb == null)
            {
                return null;
            }

            comb.Nombre = model.Nombre;
            return Update(comb);
        }
    }
}
