﻿using ProyectoFinal.DataModel.Entities;
using ProyectoFinal.Repositorio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoFinal
{
    public partial class frmCliente : Form
    {
        ClienteRepository cliente = new ClienteRepository();
        public frmCliente()
        {
            InitializeComponent();
            dgvCliente.DataSource = cliente.GetAll(); ;
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void frmCliente_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'proyectoFinalDataSet4.Cliente' Puede moverla o quitarla según sea necesario.
            this.clienteTableAdapter.Fill(this.proyectoFinalDataSet4.Cliente);

        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtNombre.Text))
            {
                MessageBox.Show("Complete todos los campos.");
            }
            else
            {
                Cliente Cliente = new Cliente()
                {

                    Nombre = txtNombre.Text,
                    Apellido = txtApellido.Text,
                    Direccion = txtDireccion.Text,
                    Telefono = txtTelefono.Text,
                    Correo = txtCorreo.Text,
                    Cedula = txtCedula.Text,
                    Estatus = "A",
                    Borrado = false,
                    FechaRegistro = System.DateTime.Now,
                    FechaModificación = System.DateTime.Now
                };


                var created = cliente.Create(Cliente);

                if (created.Nombre != null)
                {
                    MessageBox.Show("Exitoso");
                }
                else
                {
                    MessageBox.Show("Error creando la marca");
                }

                dgvCliente.DataSource = cliente.GetAll();
            }
        }

        private void dgvCliente_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            dgvCliente.DataSource = cliente.GetAll(); ;
            txtNombre.Text = dgvCliente.CurrentRow.Cells["nombreDataGridViewTextBoxColumn"].Value.ToString();
            textBoxId.Text = dgvCliente.CurrentRow.Cells["clienteIdDataGridViewTextBoxColumn"].Value.ToString();
            txtApellido.Text = dgvCliente.CurrentRow.Cells["apellidoDataGridViewTextBoxColumn"].Value.ToString();
            txtDireccion.Text = dgvCliente.CurrentRow.Cells["direccionDataGridViewTextBoxColumn"].Value.ToString();
            txtTelefono.Text = dgvCliente.CurrentRow.Cells["telefonoDataGridViewTextBoxColumn"].Value.ToString();
            txtCedula.Text = dgvCliente.CurrentRow.Cells["cedulaDataGridViewTextBoxColumn"].Value.ToString();
            txtCorreo.Text = dgvCliente.CurrentRow.Cells["correoDataGridViewTextBoxColumn"].Value.ToString();
        }

        private void txtNombre_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnBorrar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtNombre.Text))
            {
                MessageBox.Show("Complete todos los campos.");
            }
            else
            {

                var buscar = cliente.FindById(Int32.Parse(textBoxId.Text));

                var created = cliente.Delete(buscar);


                if (created.Success == true)
                {
                    MessageBox.Show("Exitoso");
                    dgvCliente.DataSource = cliente.GetAll(); ;
                }
                else
                {
                    MessageBox.Show("Error Actualizando");
                }
            }
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtNombre.Text))
            {
                MessageBox.Show("Complete todos los campos.");
            }
            else
            {
                Cliente Cliente = new Cliente()
                {
                    Id = Int32.Parse(textBoxId.Text),
                    Nombre = txtNombre.Text,
                    Apellido = txtApellido.Text,
                    Direccion = txtDireccion.Text,
                    Telefono = txtTelefono.Text,
                    Correo = txtCorreo.Text,
                    Cedula = txtCedula.Text,
                    Estatus = "A",
                    Borrado = false,
                    FechaRegistro = System.DateTime.Now,
                    FechaModificación = System.DateTime.Now
                };

                var created = cliente.UpdateCliente(Cliente);


                if (created.Success == true)
                {
                    MessageBox.Show("Exitoso");
                    dgvCliente.DataSource = cliente.GetAll(); ;
                }
                else
                {
                    MessageBox.Show("Error Actualizando");
                }
              
            }
        }
    }
}
