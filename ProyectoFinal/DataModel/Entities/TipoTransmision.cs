﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoFinal.DataModel.Entities
{
    public class TipoTransmision : BaseEntity
    {
        public string Nombre { get; set; }

        public ICollection<Vehiculo> Vehiculos { get; set; }
    }
}
