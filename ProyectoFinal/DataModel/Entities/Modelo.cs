﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoFinal.DataModel.Entities
{
    public class Modelo : BaseEntity
    {
        public int MarcaId { get; set; }
        public string Nombre { get; set; }
        public Marca Marca { get; set; }
        public ICollection<Vehiculo> Vehiculos { get; set; }
        public ICollection<Marca> Marcas { get; set; }
    }
}
