﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoFinal.DataModel.Entities
{
    public class Alquiler : BaseEntity
    {

        public int ClienteId { get; set; }
        public int VehiculoId { get; set; }
        public string MetodoPago { get; set; }
        public decimal Costo { get; set; }
        public  DateTime FechaDesde { get; set; }
        public  DateTime FechaHasta { get; set; }
        public DateTime FechaDevolucion { get; set; }
        public decimal Penalidad { get; set; }
        public Cliente Cliente { get; set; }
        public Vehiculo Vehiculo { get; set; }


    }




}
