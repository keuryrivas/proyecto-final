﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using ProyectoFinal.DataModel.Entities;

namespace ProyectoFinal.DataModel.Context
{
    public class MyAppDbContext : DbContext
    {
        public MyAppDbContext()
             : base("name=mssql")
        {

        }
        
        public DbSet<Alquiler> Alquilers { get; set; }
        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<Marca> Marcas  { get; set; }
        public DbSet<Modelo> Modelo { get; set; }
        public DbSet<TipoCombustible> TipoCombustibles { get; set; }
        public DbSet<TipoTransmision> TipoTransmisions { get; set; }
        public DbSet<Vehiculo> Vehiculos { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            #region Cliente
            modelBuilder.Entity<Cliente>()
                .ToTable("Cliente")
                .HasKey(k => k.Id);

            modelBuilder.Entity<Cliente>()
                .Property(p => p.Id)
                .HasColumnName("ClienteId");

            modelBuilder.Entity<Cliente>()
                .Property(p => p.Nombre)
                .HasMaxLength(100)
                .HasColumnType("varchar")
                .IsRequired();

            modelBuilder.Entity<Cliente>()
               .Property(p => p.Apellido)
               .HasMaxLength(100)
               .HasColumnType("varchar")
               .IsRequired();

            modelBuilder.Entity<Cliente>()
               .Property(p => p.Direccion)
               .HasMaxLength(500)
               .HasColumnType("varchar")
               .IsRequired();

            modelBuilder.Entity<Cliente>()
               .Property(p => p.Telefono)
               .HasMaxLength(20)
               .HasColumnType("varchar")
               .IsRequired();

            modelBuilder.Entity<Cliente>()
               .Property(p => p.Correo)
               .HasMaxLength(100)
               .HasColumnType("varchar")
               .IsRequired();

            modelBuilder.Entity<Cliente>()
               .Property(p => p.Cedula)
               .HasMaxLength(11)
               .HasColumnType("varchar")
               .IsRequired();

            modelBuilder.Entity<Cliente>()
                .Property(p => p.Estatus)
                .HasMaxLength(2)
                .HasColumnType("varchar")
                .IsRequired();

            modelBuilder.Entity<Cliente>()
                .Property(p => p.Borrado)
                .HasColumnType("bit")
                .IsRequired();

            modelBuilder.Entity<Cliente>()
                .Property(p => p.FechaRegistro)
                .HasColumnType("DateTime")
                .IsRequired();

            modelBuilder.Entity<Cliente>()
                .Property(p => p.FechaModificación)
                .HasColumnType("DateTime")
                .IsRequired();

            #endregion

            #region Vehiculo

            modelBuilder.Entity<Vehiculo>()
                .ToTable("Vehiculo")
                .HasKey(k => k.Id);

            modelBuilder.Entity<Vehiculo>()
                .Property(p => p.Id)
                .HasColumnName("VehiculoId");

            modelBuilder.Entity<Vehiculo>()
               .Property(p => p.Chasis)
               .HasMaxLength(100)
               .HasColumnType("varchar")
               .IsRequired();

            modelBuilder.Entity<Vehiculo>()
               .Property(p => p.Placa)
               .HasMaxLength(20)
               .HasColumnType("varchar")
               .IsRequired();

            modelBuilder.Entity<Vehiculo>()
               .Property(p => p.Anio)
               .HasMaxLength(4)
               .HasColumnType("varchar")
               .IsRequired();

            modelBuilder.Entity<Vehiculo>()
               .Property(p => p.Color)
               .HasMaxLength(20)
               .HasColumnType("varchar")
               .IsRequired();

            modelBuilder.Entity<Vehiculo>()
               .Property(p => p.Cilindraje)
               .HasMaxLength(10)
               .HasColumnType("varchar")
               .IsRequired();

            modelBuilder.Entity<Vehiculo>()
               .Property(p => p.KilometrosTablero)
               .HasMaxLength(50)
               .HasColumnType("varchar")
               .IsRequired();

            modelBuilder.Entity<Vehiculo>()
               .Property(p => p.CantidadPuerta)
               .HasColumnType("int")
               .IsRequired();

            modelBuilder.Entity<Vehiculo>()
               .Property(p => p.Precio)
               .HasColumnType("decimal")
               .IsRequired();

            modelBuilder.Entity<Vehiculo>()
                .Property(p => p.Estatus)
                .HasMaxLength(2)
                .HasColumnType("varchar")
                .IsRequired();

            modelBuilder.Entity<Vehiculo>()
                .Property(p => p.Borrado)
                .HasColumnType("bit")
                .IsRequired();

            modelBuilder.Entity<Vehiculo>()
                .Property(p => p.FechaRegistro)
                .HasColumnType("DateTime")
                .IsRequired();

            modelBuilder.Entity<Vehiculo>()
                .Property(p => p.FechaModificación)
                .HasColumnType("DateTime")
                .IsRequired();
            #endregion

            #region TipoTransmision
            modelBuilder.Entity<TipoTransmision>()
                .ToTable("TipoTransmision")
                .HasKey(k => k.Id);

            modelBuilder.Entity<TipoTransmision>()
                .Property(p => p.Id)
                .HasColumnName("TipoTransmisionId");

            modelBuilder.Entity<TipoTransmision>()
                .Property(p => p.Nombre)
                .HasMaxLength(100)
                .HasColumnType("varchar")
                .IsRequired();

            modelBuilder.Entity<TipoTransmision>()
                .Property(p => p.Estatus)
                .HasMaxLength(2)
                .HasColumnType("varchar")
                .IsRequired();

            modelBuilder.Entity<TipoTransmision>()
                .Property(p => p.Borrado)
                .HasColumnType("bit")
                .IsRequired();

            modelBuilder.Entity<TipoTransmision>()
                .Property(p => p.FechaRegistro)
                .HasColumnType("DateTime")
                .IsRequired();

            modelBuilder.Entity<TipoTransmision>()
                .Property(p => p.FechaModificación)
                .HasColumnType("DateTime")
                .IsRequired();

            #endregion

            #region TipoCombustible
            modelBuilder.Entity<TipoCombustible>()
                .ToTable("TipoCombustible")
                .HasKey(k => k.Id);

            modelBuilder.Entity<TipoCombustible>()
                .Property(p => p.Id)
                .HasColumnName("TipoCombustibleId");

            modelBuilder.Entity<TipoCombustible>()
                .Property(p => p.Nombre)
                .HasMaxLength(20)
                .HasColumnType("varchar")
                .IsRequired();

            modelBuilder.Entity<TipoCombustible>()
                .Property(p => p.Estatus)
                .HasMaxLength(2)
                .HasColumnType("varchar")
                .IsRequired();

            modelBuilder.Entity<TipoCombustible>()
                .Property(p => p.Borrado)
                .HasColumnType("bit")
                .IsRequired();

            modelBuilder.Entity<TipoCombustible>()
                .Property(p => p.FechaRegistro)
                .HasColumnType("DateTime")
                .IsRequired();

            modelBuilder.Entity<TipoCombustible>()
                .Property(p => p.FechaModificación)
                .HasColumnType("DateTime")
                .IsRequired();
            #endregion

            #region Marca
            modelBuilder.Entity<Marca>()
                .ToTable("Marca")
                .HasKey(k => k.Id);

            modelBuilder.Entity<Marca>()
                .Property(p => p.Id)
                .HasColumnName("MarcaId");

            modelBuilder.Entity<Marca>()
                .Property(p => p.Nombre)
                .HasMaxLength(100)
                .HasColumnType("varchar")
                .IsRequired();

            modelBuilder.Entity<Marca>()
                .Property(p => p.Estatus)
                .HasMaxLength(2)
                .HasColumnType("varchar")
                .IsRequired();

            modelBuilder.Entity<Marca>()
                .Property(p => p.Borrado)
                .HasColumnType("bit")
                .IsRequired();

            modelBuilder.Entity<Marca>()
                .Property(p => p.FechaRegistro)
                .HasColumnType("DateTime")
                .IsRequired();

            modelBuilder.Entity<Marca>()
                .Property(p => p.FechaModificación)
                .HasColumnType("DateTime")
                .IsRequired();
            #endregion

            #region Modelo
            modelBuilder.Entity<Modelo>()
                .ToTable("Modelo")
                .HasKey(k => k.Id);

            modelBuilder.Entity<Modelo>()
                .Property(p => p.Id)
                .HasColumnName("ModeloId");

            modelBuilder.Entity<Modelo>()
                .Property(p => p.Nombre)
                .HasMaxLength(100)
                .HasColumnType("varchar")
                .IsRequired();

            modelBuilder.Entity<Modelo>()
                .Property(p => p.Estatus)
                .HasMaxLength(2)
                .HasColumnType("varchar")
                .IsRequired();

            modelBuilder.Entity<Modelo>()
                .Property(p => p.Borrado)
                .HasColumnType("bit")
                .IsRequired();

            modelBuilder.Entity<Modelo>()
                .Property(p => p.FechaRegistro)
                .HasColumnType("DateTime")
                .IsRequired();

            modelBuilder.Entity<Modelo>()
                .Property(p => p.FechaModificación)
                .HasColumnType("DateTime")
                .IsRequired();
            #endregion

            #region Alquiler
            modelBuilder.Entity<Alquiler>()
                .ToTable("Alquiler")
                .HasKey(k => k.Id);

            modelBuilder.Entity<Alquiler>()
                .Property(p => p.Id)
                .HasColumnName("AlquilerId");

            modelBuilder.Entity<Alquiler>()
                .Property(p => p.MetodoPago)
                .HasMaxLength(50)
                .HasColumnType("varchar")
                .IsRequired();

            modelBuilder.Entity<Alquiler>()
               .Property(p => p.Costo)
               .HasColumnType("decimal")
               .IsRequired();

            modelBuilder.Entity<Alquiler>()
                .Property(p => p.FechaDesde)
                .HasColumnType("DateTime")
                .IsRequired();

            modelBuilder.Entity<Alquiler>()
                .Property(p => p.FechaHasta)
                .HasColumnType("DateTime")
                .IsRequired();

            modelBuilder.Entity<Alquiler>()
                .Property(p => p.FechaDevolucion)
                .HasColumnType("DateTime")
                .IsRequired();

            modelBuilder.Entity<Alquiler>()
               .Property(p => p.Penalidad)
               .HasColumnType("decimal")
               .IsRequired();

            modelBuilder.Entity<Alquiler>()
                .Property(p => p.Estatus)
                .HasMaxLength(2)
                .HasColumnType("varchar")
                .IsRequired();

            modelBuilder.Entity<Alquiler>()
                .Property(p => p.Borrado)
                .HasColumnType("bit")
                .IsRequired();

            modelBuilder.Entity<Alquiler>()
                .Property(p => p.FechaRegistro)
                .HasColumnType("DateTime")
                .IsRequired();

            modelBuilder.Entity<Alquiler>()
                .Property(p => p.FechaModificación)
                .HasColumnType("DateTime")
                .IsRequired();
            #endregion
        }
    }
    }
