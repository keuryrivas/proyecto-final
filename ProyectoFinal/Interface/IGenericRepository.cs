﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using ProyectoFinal.DataModel.Entities;

namespace ProyectoFinal.Interface
{
    public interface IGenericRepository<T>
    {
        T Create(T model);
        OperationResult Update(T model);
        OperationResult Delete(T model);
        List<T> GetAll();
        T FindById(int id);
    }
}
