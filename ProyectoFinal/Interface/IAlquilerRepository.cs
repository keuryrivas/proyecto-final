﻿using ProyectoFinal.DataModel.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoFinal.Interface
{
    public interface IAlquilerRepository : IGenericRepository<Alquiler>
    {
        OperationResult UpdateAlquiler(Alquiler model);
    }
}
