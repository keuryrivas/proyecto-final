﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoFinal
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void marcaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMarca marca = new frmMarca();
            marca.ShowDialog();
        }

        private void modeloToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmModelo modelo = new frmModelo();
            modelo.ShowDialog();
        }

        private void combustibleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmCombustible combustible = new frmCombustible();
            combustible.ShowDialog();
        }

        private void transmisionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmTransmision transmision = new frmTransmision();
            transmision.ShowDialog();
        }

        private void btnUsuario_Click(object sender, EventArgs e)
        {
             Mantenimiento mante = new Mantenimiento();
            mante.ShowDialog();
        }

        private void btnAlquilar_Click(object sender, EventArgs e)
        {
            frmAlquiler alquiler = new frmAlquiler();
            alquiler.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmMarca fmarca = new frmMarca();
            fmarca.ShowDialog();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
