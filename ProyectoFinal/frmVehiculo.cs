﻿using ProyectoFinal.DataModel.Entities;
using ProyectoFinal.Repositorio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoFinal
{
    public partial class frmVehiculo : Form
    {

        VehiculoRepository vehiculo = new VehiculoRepository();
        TipoTransmisionRepository tt = new TipoTransmisionRepository();
        TipoCombustibleRepository tc = new TipoCombustibleRepository();
        ModeloRepository md = new ModeloRepository();

        public frmVehiculo()
        {
            InitializeComponent();
        }

        private void frmVehiculo_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'proyectoFinalDataSet7.Vehiculo' Puede moverla o quitarla según sea necesario.
            this.vehiculoTableAdapter.Fill(this.proyectoFinalDataSet7.Vehiculo);
            dataGridView1.DataSource = vehiculo.GetAll();

        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(textBoxId.Text))
            {
                MessageBox.Show("Complete todos los campos.");
            }
            else
            {

                var buscar = vehiculo.FindById(Int32.Parse(textBoxId.Text));

                var created = vehiculo.Delete(buscar);


                if (created.Success == true)
                {
                    MessageBox.Show("Exitoso");
                    dataGridView1.DataSource = vehiculo.GetAll();
                }
                else
                {
                    MessageBox.Show("Error Actualizando");
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(textChasis.Text) || string.IsNullOrWhiteSpace(textPlaca.Text) || string.IsNullOrWhiteSpace(textAnio.Text) || string.IsNullOrWhiteSpace(textColor.Text) || string.IsNullOrWhiteSpace(textCilindraje.Text) || string.IsNullOrWhiteSpace(textKilometros.Text) || string.IsNullOrWhiteSpace(textCP.Text) || string.IsNullOrWhiteSpace(textPrecio.Text) || string.IsNullOrWhiteSpace(textTT.Text) || string.IsNullOrWhiteSpace(textTC.Text) || string.IsNullOrWhiteSpace(textModeloId.Text))
            {
                MessageBox.Show("Complete todos los campos.");
            }
            else
            {
                Vehiculo vh = new Vehiculo()
                {
                    TipoTransmisionId = Int32.Parse(textTT.Text),
                    TipoCombustibleId = Int32.Parse(textTC.Text),
                    ModeloId = Int32.Parse(textModeloId.Text),
                    Chasis = textChasis.Text,
                    Placa = textPlaca.Text,
                    Anio = textAnio.Text,
                    Color = textColor.Text,
                    Cilindraje = textCilindraje.Text,
                    KilometrosTablero = textKilometros.Text,
                    CantidadPuerta = Int32.Parse(textCP.Text),
                    Precio = Decimal.Parse(textPrecio.Text),
                    Estatus = "A",
                    Borrado = false,
                    FechaRegistro = System.DateTime.Now,
                    FechaModificación = System.DateTime.Now
                };


                var created = vehiculo.Create(vh);

                if (created.Chasis != null)
                {
                    MessageBox.Show("Exitoso");
                }
                else
                {
                    MessageBox.Show("Error creando la marca");
                }

                dataGridView1.DataSource = vehiculo.GetAll();
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            textTT.Text = dataGridView1.CurrentRow.Cells["tipoTransmisionIdDataGridViewTextBoxColumn"].Value.ToString();
            textTC.Text = dataGridView1.CurrentRow.Cells["tipoCombustibleIdDataGridViewTextBoxColumn"].Value.ToString();
            textModeloId.Text = dataGridView1.CurrentRow.Cells["modeloIdDataGridViewTextBoxColumn"].Value.ToString();
            textChasis.Text = dataGridView1.CurrentRow.Cells["chasisDataGridViewTextBoxColumn"].Value.ToString();
            textPlaca.Text = dataGridView1.CurrentRow.Cells["placaDataGridViewTextBoxColumn"].Value.ToString();
            textAnio.Text = dataGridView1.CurrentRow.Cells["anioDataGridViewTextBoxColumn"].Value.ToString();
            textColor.Text = dataGridView1.CurrentRow.Cells["colorDataGridViewTextBoxColumn"].Value.ToString();
            textCilindraje.Text = dataGridView1.CurrentRow.Cells["cilindrajeDataGridViewTextBoxColumn"].Value.ToString();
            textKilometros.Text = dataGridView1.CurrentRow.Cells["kilometrosTableroDataGridViewTextBoxColumn"].Value.ToString();
            textCP.Text = dataGridView1.CurrentRow.Cells["cantidadPuertaDataGridViewTextBoxColumn"].Value.ToString();
            textPrecio.Text = dataGridView1.CurrentRow.Cells["precioDataGridViewTextBoxColumn"].Value.ToString();
            textBoxId.Text = dataGridView1.CurrentRow.Cells["vehiculoIdDataGridViewTextBoxColumn"].Value.ToString();
        }

        private void Buscartt_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(textTT.Text))
            {
                MessageBox.Show("Complete el campo para buscar.");
            }
            else
            {

                var buscar = tt.FindById(Int32.Parse(textTT.Text));


                if (buscar != null)
                {
                    textDesTT.Text = buscar.Nombre;
                    textTT.Text = buscar.Id.ToString();

                }
                else
                {
                    MessageBox.Show("Transmision no encontrada");
                }
            }
        }

        private void Buscartc_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(textTT.Text))
            {
                MessageBox.Show("Complete el campo para buscar.");
            }
            else
            {

                var buscar = tc.FindById(Int32.Parse(textTT.Text));


                if (buscar != null)
                {
                    textDesTC.Text = buscar.Nombre;
                    textTC.Text = buscar.Id.ToString();

                }
                else
                {
                    MessageBox.Show("Transmision no encontrada");
                }
            }
        }

        private void BuscarM_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(textTT.Text))
            {
                MessageBox.Show("Complete el campo para buscar.");
            }
            else
            {

                var buscar = md.FindById(Int32.Parse(textTT.Text));


                if (buscar != null)
                {
                    textDesModelo.Text = buscar.Nombre;
                    textModeloId.Text = buscar.Id.ToString();

                }
                else
                {
                    MessageBox.Show("Transmision no encontrada");
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(textChasis.Text) || string.IsNullOrWhiteSpace(textPlaca.Text) || string.IsNullOrWhiteSpace(textAnio.Text) || string.IsNullOrWhiteSpace(textColor.Text) || string.IsNullOrWhiteSpace(textCilindraje.Text) || string.IsNullOrWhiteSpace(textKilometros.Text) || string.IsNullOrWhiteSpace(textCP.Text) || string.IsNullOrWhiteSpace(textPrecio.Text) || string.IsNullOrWhiteSpace(textTT.Text) || string.IsNullOrWhiteSpace(textTC.Text) || string.IsNullOrWhiteSpace(textModeloId.Text))
            {
                MessageBox.Show("Complete todos los campos.");
            }
            else
            {
                Vehiculo vh = new Vehiculo()
                {
                    Id = Int32.Parse(textBoxId.Text),
                    TipoTransmisionId = Int32.Parse(textTT.Text),
                    TipoCombustibleId = Int32.Parse(textTC.Text),
                    ModeloId = Int32.Parse(textModeloId.Text),
                    Chasis = textChasis.Text,
                    Placa = textPlaca.Text,
                    Anio = textAnio.Text,
                    Color = textColor.Text,
                    Cilindraje = textCilindraje.Text,
                    KilometrosTablero = textKilometros.Text,
                    CantidadPuerta = Int32.Parse(textCP.Text),
                    Precio = Decimal.Parse(textPrecio.Text),
                    Estatus = "A",
                    Borrado = false,
                    FechaRegistro = System.DateTime.Now,
                    FechaModificación = System.DateTime.Now
                };

                var created = vehiculo.UpdateVehiculo(vh);


                if (created.Success == true)
                {
                    MessageBox.Show("Exitoso");
                    dataGridView1.DataSource = vehiculo.GetAll();
                }
                else
                {
                    MessageBox.Show("Error Actualizando");
                }

            }
        }
    }
}
