﻿using ProyectoFinal.DataModel.Entities;
using ProyectoFinal.Interface;
using ProyectoFinal.Repositorio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoFinal
{

    public partial class frmMarca : Form
    {

        MarcaRepository mr = new MarcaRepository();

        public frmMarca()
        {

            InitializeComponent();
            dataGridViewMarca.DataSource = mr.GetAll();
        }

        private void dgvMarca_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            txtNombre.Text = dataGridViewMarca.CurrentRow.Cells["nombreDataGridViewTextBoxColumn"].Value.ToString();
            textBoxId.Text = dataGridViewMarca.CurrentRow.Cells["idDataGridViewTextBoxColumn"].Value.ToString();

        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtNombre.Text))
            {
                MessageBox.Show("Complete todos los campos.");
            }
            else
            {
                Marca marca = new Marca()
                {

                    Nombre = txtNombre.Text,
                    Estatus = "A",
                    Borrado = false,
                    FechaRegistro = System.DateTime.Now,
                    FechaModificación = System.DateTime.Now
                };


                var created = mr.Create(marca);

                if (created.Nombre != null)
                {
                    MessageBox.Show("Exitoso");
                }
                else
                {
                    MessageBox.Show("Error creando la marca");
                }

                dataGridViewMarca.DataSource = mr.GetAll();
            }

        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtNombre.Text))
            {
                MessageBox.Show("Complete todos los campos.");
            }
            else
            {
                Marca marca = new Marca()
                {
                    Id = Int32.Parse(textBoxId.Text),
                    Nombre = txtNombre.Text,
                    Estatus = "A",
                    Borrado = false,
                    FechaRegistro = System.DateTime.Now,
                    FechaModificación = System.DateTime.Now
                };

                var created = mr.UpdateMarca(marca);


                if (created.Success == true)
                {
                    MessageBox.Show("Exitoso");
                    dataGridViewMarca.DataSource = mr.GetAll();
                }
                else
                {
                    MessageBox.Show("Error Actualizando");
                }

            }
        }

        private void txtNombre_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void btnBorrar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtNombre.Text))
            {
                MessageBox.Show("Complete todos los campos.");
            }
            else
            {

                var buscar = mr.FindById(Int32.Parse(textBoxId.Text));

                var created = mr.Delete(buscar);


                if (created.Success == true)
                {
                    MessageBox.Show("Exitoso");
                    dataGridViewMarca.DataSource = mr.GetAll();
                }
                else
                {
                    MessageBox.Show("Error Actualizando");
                }
            }
        }

        private void frmMarca_Load(object sender, EventArgs e)
        {

        }
    }
}
