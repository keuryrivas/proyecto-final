﻿
namespace ProyectoFinal
{
    partial class frmVehiculo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.vehiculoIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tipoTransmisionIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tipoCombustibleIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.modeloIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.chasisDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.placaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.anioDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colorDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cilindrajeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kilometrosTableroDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cantidadPuertaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.precioDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.estatusDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fechaRegistroDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fechaModificaciónDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vehiculoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.proyectoFinalDataSet7 = new ProyectoFinal.ProyectoFinalDataSet7();
            this.vehiculoTableAdapter = new ProyectoFinal.ProyectoFinalDataSet7TableAdapters.VehiculoTableAdapter();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.textChasis = new System.Windows.Forms.TextBox();
            this.textPlaca = new System.Windows.Forms.TextBox();
            this.textAnio = new System.Windows.Forms.TextBox();
            this.textColor = new System.Windows.Forms.TextBox();
            this.textCilindraje = new System.Windows.Forms.TextBox();
            this.textCP = new System.Windows.Forms.TextBox();
            this.textPrecio = new System.Windows.Forms.TextBox();
            this.textTT = new System.Windows.Forms.TextBox();
            this.textTC = new System.Windows.Forms.TextBox();
            this.textModeloId = new System.Windows.Forms.TextBox();
            this.textKilometros = new System.Windows.Forms.TextBox();
            this.textBoxId = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.textDesTT = new System.Windows.Forms.TextBox();
            this.textDesTC = new System.Windows.Forms.TextBox();
            this.textDesModelo = new System.Windows.Forms.TextBox();
            this.Buscartt = new System.Windows.Forms.Button();
            this.Buscartc = new System.Windows.Forms.Button();
            this.BuscarM = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vehiculoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.proyectoFinalDataSet7)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.vehiculoIdDataGridViewTextBoxColumn,
            this.tipoTransmisionIdDataGridViewTextBoxColumn,
            this.tipoCombustibleIdDataGridViewTextBoxColumn,
            this.modeloIdDataGridViewTextBoxColumn,
            this.chasisDataGridViewTextBoxColumn,
            this.placaDataGridViewTextBoxColumn,
            this.anioDataGridViewTextBoxColumn,
            this.colorDataGridViewTextBoxColumn,
            this.cilindrajeDataGridViewTextBoxColumn,
            this.kilometrosTableroDataGridViewTextBoxColumn,
            this.cantidadPuertaDataGridViewTextBoxColumn,
            this.precioDataGridViewTextBoxColumn,
            this.estatusDataGridViewTextBoxColumn,
            this.fechaRegistroDataGridViewTextBoxColumn,
            this.fechaModificaciónDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.vehiculoBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(12, 271);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(776, 150);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // vehiculoIdDataGridViewTextBoxColumn
            // 
            this.vehiculoIdDataGridViewTextBoxColumn.DataPropertyName = "VehiculoId";
            this.vehiculoIdDataGridViewTextBoxColumn.HeaderText = "VehiculoId";
            this.vehiculoIdDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.vehiculoIdDataGridViewTextBoxColumn.Name = "vehiculoIdDataGridViewTextBoxColumn";
            this.vehiculoIdDataGridViewTextBoxColumn.ReadOnly = true;
            this.vehiculoIdDataGridViewTextBoxColumn.Width = 125;
            // 
            // tipoTransmisionIdDataGridViewTextBoxColumn
            // 
            this.tipoTransmisionIdDataGridViewTextBoxColumn.DataPropertyName = "TipoTransmisionId";
            this.tipoTransmisionIdDataGridViewTextBoxColumn.HeaderText = "TipoTransmisionId";
            this.tipoTransmisionIdDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.tipoTransmisionIdDataGridViewTextBoxColumn.Name = "tipoTransmisionIdDataGridViewTextBoxColumn";
            this.tipoTransmisionIdDataGridViewTextBoxColumn.Width = 125;
            // 
            // tipoCombustibleIdDataGridViewTextBoxColumn
            // 
            this.tipoCombustibleIdDataGridViewTextBoxColumn.DataPropertyName = "TipoCombustibleId";
            this.tipoCombustibleIdDataGridViewTextBoxColumn.HeaderText = "TipoCombustibleId";
            this.tipoCombustibleIdDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.tipoCombustibleIdDataGridViewTextBoxColumn.Name = "tipoCombustibleIdDataGridViewTextBoxColumn";
            this.tipoCombustibleIdDataGridViewTextBoxColumn.Width = 125;
            // 
            // modeloIdDataGridViewTextBoxColumn
            // 
            this.modeloIdDataGridViewTextBoxColumn.DataPropertyName = "ModeloId";
            this.modeloIdDataGridViewTextBoxColumn.HeaderText = "ModeloId";
            this.modeloIdDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.modeloIdDataGridViewTextBoxColumn.Name = "modeloIdDataGridViewTextBoxColumn";
            this.modeloIdDataGridViewTextBoxColumn.Width = 125;
            // 
            // chasisDataGridViewTextBoxColumn
            // 
            this.chasisDataGridViewTextBoxColumn.DataPropertyName = "Chasis";
            this.chasisDataGridViewTextBoxColumn.HeaderText = "Chasis";
            this.chasisDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.chasisDataGridViewTextBoxColumn.Name = "chasisDataGridViewTextBoxColumn";
            this.chasisDataGridViewTextBoxColumn.Width = 125;
            // 
            // placaDataGridViewTextBoxColumn
            // 
            this.placaDataGridViewTextBoxColumn.DataPropertyName = "Placa";
            this.placaDataGridViewTextBoxColumn.HeaderText = "Placa";
            this.placaDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.placaDataGridViewTextBoxColumn.Name = "placaDataGridViewTextBoxColumn";
            this.placaDataGridViewTextBoxColumn.Width = 125;
            // 
            // anioDataGridViewTextBoxColumn
            // 
            this.anioDataGridViewTextBoxColumn.DataPropertyName = "Anio";
            this.anioDataGridViewTextBoxColumn.HeaderText = "Anio";
            this.anioDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.anioDataGridViewTextBoxColumn.Name = "anioDataGridViewTextBoxColumn";
            this.anioDataGridViewTextBoxColumn.Width = 125;
            // 
            // colorDataGridViewTextBoxColumn
            // 
            this.colorDataGridViewTextBoxColumn.DataPropertyName = "Color";
            this.colorDataGridViewTextBoxColumn.HeaderText = "Color";
            this.colorDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.colorDataGridViewTextBoxColumn.Name = "colorDataGridViewTextBoxColumn";
            this.colorDataGridViewTextBoxColumn.Width = 125;
            // 
            // cilindrajeDataGridViewTextBoxColumn
            // 
            this.cilindrajeDataGridViewTextBoxColumn.DataPropertyName = "Cilindraje";
            this.cilindrajeDataGridViewTextBoxColumn.HeaderText = "Cilindraje";
            this.cilindrajeDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.cilindrajeDataGridViewTextBoxColumn.Name = "cilindrajeDataGridViewTextBoxColumn";
            this.cilindrajeDataGridViewTextBoxColumn.Width = 125;
            // 
            // kilometrosTableroDataGridViewTextBoxColumn
            // 
            this.kilometrosTableroDataGridViewTextBoxColumn.DataPropertyName = "KilometrosTablero";
            this.kilometrosTableroDataGridViewTextBoxColumn.HeaderText = "KilometrosTablero";
            this.kilometrosTableroDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.kilometrosTableroDataGridViewTextBoxColumn.Name = "kilometrosTableroDataGridViewTextBoxColumn";
            this.kilometrosTableroDataGridViewTextBoxColumn.Width = 125;
            // 
            // cantidadPuertaDataGridViewTextBoxColumn
            // 
            this.cantidadPuertaDataGridViewTextBoxColumn.DataPropertyName = "CantidadPuerta";
            this.cantidadPuertaDataGridViewTextBoxColumn.HeaderText = "CantidadPuerta";
            this.cantidadPuertaDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.cantidadPuertaDataGridViewTextBoxColumn.Name = "cantidadPuertaDataGridViewTextBoxColumn";
            this.cantidadPuertaDataGridViewTextBoxColumn.Width = 125;
            // 
            // precioDataGridViewTextBoxColumn
            // 
            this.precioDataGridViewTextBoxColumn.DataPropertyName = "Precio";
            this.precioDataGridViewTextBoxColumn.HeaderText = "Precio";
            this.precioDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.precioDataGridViewTextBoxColumn.Name = "precioDataGridViewTextBoxColumn";
            this.precioDataGridViewTextBoxColumn.Width = 125;
            // 
            // estatusDataGridViewTextBoxColumn
            // 
            this.estatusDataGridViewTextBoxColumn.DataPropertyName = "Estatus";
            this.estatusDataGridViewTextBoxColumn.HeaderText = "Estatus";
            this.estatusDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.estatusDataGridViewTextBoxColumn.Name = "estatusDataGridViewTextBoxColumn";
            this.estatusDataGridViewTextBoxColumn.Width = 125;
            // 
            // fechaRegistroDataGridViewTextBoxColumn
            // 
            this.fechaRegistroDataGridViewTextBoxColumn.DataPropertyName = "FechaRegistro";
            this.fechaRegistroDataGridViewTextBoxColumn.HeaderText = "FechaRegistro";
            this.fechaRegistroDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.fechaRegistroDataGridViewTextBoxColumn.Name = "fechaRegistroDataGridViewTextBoxColumn";
            this.fechaRegistroDataGridViewTextBoxColumn.Width = 125;
            // 
            // fechaModificaciónDataGridViewTextBoxColumn
            // 
            this.fechaModificaciónDataGridViewTextBoxColumn.DataPropertyName = "FechaModificación";
            this.fechaModificaciónDataGridViewTextBoxColumn.HeaderText = "FechaModificación";
            this.fechaModificaciónDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.fechaModificaciónDataGridViewTextBoxColumn.Name = "fechaModificaciónDataGridViewTextBoxColumn";
            this.fechaModificaciónDataGridViewTextBoxColumn.Width = 125;
            // 
            // vehiculoBindingSource
            // 
            this.vehiculoBindingSource.DataMember = "Vehiculo";
            this.vehiculoBindingSource.DataSource = this.proyectoFinalDataSet7;
            // 
            // proyectoFinalDataSet7
            // 
            this.proyectoFinalDataSet7.DataSetName = "ProyectoFinalDataSet7";
            this.proyectoFinalDataSet7.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // vehiculoTableAdapter
            // 
            this.vehiculoTableAdapter.ClearBeforeFill = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 230);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 35);
            this.button1.TabIndex = 1;
            this.button1.Text = "Agregar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(93, 230);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(89, 35);
            this.button2.TabIndex = 2;
            this.button2.Text = "Actualizar";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(188, 230);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(89, 35);
            this.button3.TabIndex = 3;
            this.button3.Text = "Borrar";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // textChasis
            // 
            this.textChasis.Location = new System.Drawing.Point(93, 12);
            this.textChasis.Name = "textChasis";
            this.textChasis.Size = new System.Drawing.Size(99, 22);
            this.textChasis.TabIndex = 4;
            // 
            // textPlaca
            // 
            this.textPlaca.Location = new System.Drawing.Point(93, 40);
            this.textPlaca.Name = "textPlaca";
            this.textPlaca.Size = new System.Drawing.Size(99, 22);
            this.textPlaca.TabIndex = 5;
            // 
            // textAnio
            // 
            this.textAnio.Location = new System.Drawing.Point(93, 68);
            this.textAnio.Name = "textAnio";
            this.textAnio.Size = new System.Drawing.Size(99, 22);
            this.textAnio.TabIndex = 6;
            // 
            // textColor
            // 
            this.textColor.Location = new System.Drawing.Point(93, 96);
            this.textColor.Name = "textColor";
            this.textColor.Size = new System.Drawing.Size(99, 22);
            this.textColor.TabIndex = 7;
            // 
            // textCilindraje
            // 
            this.textCilindraje.Location = new System.Drawing.Point(93, 124);
            this.textCilindraje.Name = "textCilindraje";
            this.textCilindraje.Size = new System.Drawing.Size(99, 22);
            this.textCilindraje.TabIndex = 8;
            // 
            // textCP
            // 
            this.textCP.Location = new System.Drawing.Point(382, 12);
            this.textCP.Name = "textCP";
            this.textCP.Size = new System.Drawing.Size(99, 22);
            this.textCP.TabIndex = 9;
            // 
            // textPrecio
            // 
            this.textPrecio.Location = new System.Drawing.Point(382, 40);
            this.textPrecio.Name = "textPrecio";
            this.textPrecio.Size = new System.Drawing.Size(99, 22);
            this.textPrecio.TabIndex = 10;
            // 
            // textTT
            // 
            this.textTT.Location = new System.Drawing.Point(382, 68);
            this.textTT.Name = "textTT";
            this.textTT.Size = new System.Drawing.Size(99, 22);
            this.textTT.TabIndex = 11;
            // 
            // textTC
            // 
            this.textTC.Location = new System.Drawing.Point(382, 96);
            this.textTC.Name = "textTC";
            this.textTC.Size = new System.Drawing.Size(99, 22);
            this.textTC.TabIndex = 12;
            // 
            // textModeloId
            // 
            this.textModeloId.Location = new System.Drawing.Point(382, 124);
            this.textModeloId.Name = "textModeloId";
            this.textModeloId.Size = new System.Drawing.Size(99, 22);
            this.textModeloId.TabIndex = 13;
            // 
            // textKilometros
            // 
            this.textKilometros.Location = new System.Drawing.Point(93, 152);
            this.textKilometros.Name = "textKilometros";
            this.textKilometros.Size = new System.Drawing.Size(99, 22);
            this.textKilometros.TabIndex = 14;
            // 
            // textBoxId
            // 
            this.textBoxId.Location = new System.Drawing.Point(382, 161);
            this.textBoxId.Name = "textBoxId";
            this.textBoxId.ReadOnly = true;
            this.textBoxId.Size = new System.Drawing.Size(99, 22);
            this.textBoxId.TabIndex = 15;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 17);
            this.label1.TabIndex = 16;
            this.label1.Text = "Chasis";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(36, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 17);
            this.label2.TabIndex = 17;
            this.label2.Text = "Placa";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(43, 71);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 17);
            this.label3.TabIndex = 18;
            this.label3.Text = "Anio";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(38, 99);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 17);
            this.label4.TabIndex = 19;
            this.label4.Text = "Color";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 124);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(66, 17);
            this.label5.TabIndex = 20;
            this.label5.Text = "Cilindraje";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 152);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(74, 17);
            this.label6.TabIndex = 21;
            this.label6.Text = "Kilometros";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(256, 12);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(117, 17);
            this.label7.TabIndex = 22;
            this.label7.Text = "Cantidad Puertas";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(286, 40);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(87, 17);
            this.label8.TabIndex = 23;
            this.label8.Text = "Precios RD$";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(249, 71);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(124, 17);
            this.label9.TabIndex = 24;
            this.label9.Text = "TipoTransmicionId";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(249, 101);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(124, 17);
            this.label10.TabIndex = 25;
            this.label10.Text = "TipoCombustibleId";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(308, 129);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(65, 17);
            this.label11.TabIndex = 26;
            this.label11.Text = "ModeloId";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(300, 166);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(73, 17);
            this.label12.TabIndex = 27;
            this.label12.Text = "VehiculoId";
            // 
            // textDesTT
            // 
            this.textDesTT.Location = new System.Drawing.Point(589, 66);
            this.textDesTT.Name = "textDesTT";
            this.textDesTT.ReadOnly = true;
            this.textDesTT.Size = new System.Drawing.Size(99, 22);
            this.textDesTT.TabIndex = 28;
            // 
            // textDesTC
            // 
            this.textDesTC.Location = new System.Drawing.Point(589, 94);
            this.textDesTC.Name = "textDesTC";
            this.textDesTC.ReadOnly = true;
            this.textDesTC.Size = new System.Drawing.Size(99, 22);
            this.textDesTC.TabIndex = 29;
            // 
            // textDesModelo
            // 
            this.textDesModelo.Location = new System.Drawing.Point(589, 126);
            this.textDesModelo.Name = "textDesModelo";
            this.textDesModelo.ReadOnly = true;
            this.textDesModelo.Size = new System.Drawing.Size(99, 22);
            this.textDesModelo.TabIndex = 30;
            // 
            // Buscartt
            // 
            this.Buscartt.Location = new System.Drawing.Point(699, 66);
            this.Buscartt.Name = "Buscartt";
            this.Buscartt.Size = new System.Drawing.Size(89, 22);
            this.Buscartt.TabIndex = 31;
            this.Buscartt.Text = "Buscar";
            this.Buscartt.UseVisualStyleBackColor = true;
            this.Buscartt.Click += new System.EventHandler(this.Buscartt_Click);
            // 
            // Buscartc
            // 
            this.Buscartc.Location = new System.Drawing.Point(699, 94);
            this.Buscartc.Name = "Buscartc";
            this.Buscartc.Size = new System.Drawing.Size(89, 22);
            this.Buscartc.TabIndex = 32;
            this.Buscartc.Text = "Buscar";
            this.Buscartc.UseVisualStyleBackColor = true;
            this.Buscartc.Click += new System.EventHandler(this.Buscartc_Click);
            // 
            // BuscarM
            // 
            this.BuscarM.Location = new System.Drawing.Point(699, 124);
            this.BuscarM.Name = "BuscarM";
            this.BuscarM.Size = new System.Drawing.Size(89, 22);
            this.BuscarM.TabIndex = 33;
            this.BuscarM.Text = "Buscar";
            this.BuscarM.UseVisualStyleBackColor = true;
            this.BuscarM.Click += new System.EventHandler(this.BuscarM_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(525, 69);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(58, 17);
            this.label13.TabIndex = 34;
            this.label13.Text = "Nombre";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(525, 97);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(58, 17);
            this.label14.TabIndex = 35;
            this.label14.Text = "Nombre";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(525, 127);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(58, 17);
            this.label15.TabIndex = 36;
            this.label15.Text = "Nombre";
            // 
            // frmVehiculo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.BuscarM);
            this.Controls.Add(this.Buscartc);
            this.Controls.Add(this.Buscartt);
            this.Controls.Add(this.textDesModelo);
            this.Controls.Add(this.textDesTC);
            this.Controls.Add(this.textDesTT);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxId);
            this.Controls.Add(this.textKilometros);
            this.Controls.Add(this.textModeloId);
            this.Controls.Add(this.textTC);
            this.Controls.Add(this.textTT);
            this.Controls.Add(this.textPrecio);
            this.Controls.Add(this.textCP);
            this.Controls.Add(this.textCilindraje);
            this.Controls.Add(this.textColor);
            this.Controls.Add(this.textAnio);
            this.Controls.Add(this.textPlaca);
            this.Controls.Add(this.textChasis);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dataGridView1);
            this.Name = "frmVehiculo";
            this.Text = "frmVehiculo";
            this.Load += new System.EventHandler(this.frmVehiculo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vehiculoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.proyectoFinalDataSet7)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private ProyectoFinalDataSet7 proyectoFinalDataSet7;
        private System.Windows.Forms.BindingSource vehiculoBindingSource;
        private ProyectoFinalDataSet7TableAdapters.VehiculoTableAdapter vehiculoTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn vehiculoIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tipoTransmisionIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tipoCombustibleIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn modeloIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn chasisDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn placaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn anioDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn colorDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cilindrajeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn kilometrosTableroDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cantidadPuertaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn precioDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn estatusDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fechaRegistroDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fechaModificaciónDataGridViewTextBoxColumn;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox textChasis;
        private System.Windows.Forms.TextBox textPlaca;
        private System.Windows.Forms.TextBox textAnio;
        private System.Windows.Forms.TextBox textColor;
        private System.Windows.Forms.TextBox textCilindraje;
        private System.Windows.Forms.TextBox textCP;
        private System.Windows.Forms.TextBox textPrecio;
        private System.Windows.Forms.TextBox textTT;
        private System.Windows.Forms.TextBox textTC;
        private System.Windows.Forms.TextBox textModeloId;
        private System.Windows.Forms.TextBox textKilometros;
        private System.Windows.Forms.TextBox textBoxId;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textDesTT;
        private System.Windows.Forms.TextBox textDesTC;
        private System.Windows.Forms.TextBox textDesModelo;
        private System.Windows.Forms.Button Buscartt;
        private System.Windows.Forms.Button Buscartc;
        private System.Windows.Forms.Button BuscarM;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
    }
}