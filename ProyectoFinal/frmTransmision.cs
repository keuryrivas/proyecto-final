﻿using ProyectoFinal.DataModel.Entities;
using ProyectoFinal.Repositorio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoFinal
{
    public partial class frmTransmision : Form
    {
        TipoTransmisionRepository tm = new TipoTransmisionRepository();
        public frmTransmision()
        {
            InitializeComponent();
            dgvTransmision.DataSource = tm.GetAll();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void btnBorrar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtNombre.Text))
            {
                MessageBox.Show("Complete todos los campos.");
            }
            else
            {

                var buscar = tm.FindById(Int32.Parse(textBoxId.Text));

                var created = tm.Delete(buscar);


                if (created.Success == true)
                {
                    MessageBox.Show("Exitoso");
                    dgvTransmision.DataSource = tm.GetAll();
                }
                else
                {
                    MessageBox.Show("Error Actualizando");
                }
            }
        }

        private void frmTransmision_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'proyectoFinalDataSet2.TipoTransmision' Puede moverla o quitarla según sea necesario.
            this.tipoTransmisionTableAdapter.Fill(this.proyectoFinalDataSet2.TipoTransmision);

        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtNombre.Text))
            {
                MessageBox.Show("Complete todos los campos.");
            }
            else
            {
                TipoTransmision trans = new TipoTransmision()
                {

                    Nombre = txtNombre.Text,
                    Estatus = "A",
                    Borrado = false,
                    FechaRegistro = System.DateTime.Now,
                    FechaModificación = System.DateTime.Now
                };


                var created = tm.Create(trans);

                if (created.Nombre != null)
                {
                    MessageBox.Show("Exitoso");
                }
                else
                {
                    MessageBox.Show("Error creando la marca");
                }

                dgvTransmision.DataSource = tm.GetAll();
            }
        }

        private void dgvTransmision_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            dgvTransmision.DataSource = tm.GetAll();
            txtNombre.Text = dgvTransmision.CurrentRow.Cells["nombreDataGridViewTextBoxColumn"].Value.ToString();
            textBoxId.Text = dgvTransmision.CurrentRow.Cells["tipoTransmisionIdDataGridViewTextBoxColumn"].Value.ToString();
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtNombre.Text))
            {
                MessageBox.Show("Complete todos los campos.");
            }
            else
            {
                TipoTransmision trans = new TipoTransmision()
                {
                    Id = Int32.Parse(textBoxId.Text),
                    Nombre = txtNombre.Text,
                    Estatus = "A",
                    Borrado = false,
                    FechaRegistro = System.DateTime.Now,
                    FechaModificación = System.DateTime.Now
                };

                var created = tm.UpdateTrans(trans);


                if (created.Success == true)
                {
                    MessageBox.Show("Exitoso");
                    dgvTransmision.DataSource = tm.GetAll();
                }
                else
                {
                    MessageBox.Show("Error Actualizando");
                }

            }
        }
    }
}
