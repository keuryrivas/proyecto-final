﻿using ProyectoFinal.DataModel.Entities;
using ProyectoFinal.Repositorio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoFinal
{
    public partial class frmCombustible : Form
    {
        TipoCombustibleRepository cb = new TipoCombustibleRepository();
        public frmCombustible()
        {
            InitializeComponent();
            dgvCombustible.DataSource = cb.GetAll();
        }

        private void frmCombustible_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'proyectoFinalDataSet6.TipoCombustible' Puede moverla o quitarla según sea necesario.
            this.tipoCombustibleTableAdapter.Fill(this.proyectoFinalDataSet6.TipoCombustible);
            // TODO: esta línea de código carga datos en la tabla 'proyectoFinalDataSet3.TipoTransmision' Puede moverla o quitarla según sea necesario.
            this.tipoTransmisionTableAdapter.Fill(this.proyectoFinalDataSet3.TipoTransmision);

        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtNombre.Text))
            {
                MessageBox.Show("Complete todos los campos.");
            }
            else
            {
                TipoCombustible combustible = new TipoCombustible()
                {

                    Nombre = txtNombre.Text,
                    Estatus = "A",
                    Borrado = false,
                    FechaRegistro = System.DateTime.Now,
                    FechaModificación = System.DateTime.Now
                };


                var created = cb.Create(combustible);

                if (created.Nombre != null)
                {
                    MessageBox.Show("Exitoso");
                }
                else
                {
                    MessageBox.Show("Error creando la marca");
                }

                dgvCombustible.DataSource = cb.GetAll();
            }
        }

        private void dgvCombustible_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            txtNombre.Text = dgvCombustible.CurrentRow.Cells["nombreDataGridViewTextBoxColumn"].Value.ToString();
            textBoxId.Text = dgvCombustible.CurrentRow.Cells["dataGridViewTextBoxColumn1"].Value.ToString();
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtNombre.Text))
            {
                MessageBox.Show("Complete todos los campos.");
            }
            else
            {
                TipoCombustible combustible = new TipoCombustible()
                {
                    Id = Int32.Parse(textBoxId.Text),
                    Nombre = txtNombre.Text,
                    Estatus = "A",
                    Borrado = false,
                    FechaRegistro = System.DateTime.Now,
                    FechaModificación = System.DateTime.Now
                };

                var created = cb.UpdateCombustible(combustible);


                if (created.Success == true)
                {
                    MessageBox.Show("Exitoso");
                    dgvCombustible.DataSource = cb.GetAll();
                }
                else
                {
                    MessageBox.Show("Error Actualizando");
                }

            }
        }

        private void btnBorrar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtNombre.Text))
            {
                MessageBox.Show("Complete todos los campos.");
            }
            else
            {

                var buscar = cb.FindById(Int32.Parse(textBoxId.Text));

                var created = cb.Delete(buscar);


                if (created.Success == true)
                {
                    MessageBox.Show("Exitoso");
                    dgvCombustible.DataSource = cb.GetAll();
                }
                else
                {
                    MessageBox.Show("Error Actualizando");
                }
            }
        }
    }
}
